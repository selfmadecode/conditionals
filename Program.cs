﻿using CoreFundamentals.conditionals;
using System;
using System.Collections.Generic;

namespace CoreFundamentals
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Core basics of C#";
            
            DoWhileClass.DoWhile();
            ForEachClass.forEachMethod(); // Implemeting foreach
            ForLoopClass.forLoop(); // Implementing Forloop
            IfStatement.IfStateMent(); // implementing the if statement
            WhileClass.While();
            SwitchCaseClass.SwitchCase();
            Console.ReadLine();

        }
        public static void CreateNewLine()
        {
            Console.WriteLine(Environment.NewLine);
        }
    }
}
