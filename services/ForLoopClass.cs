﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CoreFundamentals.conditionals
{
    class ForLoopClass
    {
        public static void forLoop()
        {
            var list = new ClassMemberList(); // Creates an instance of the ClassList
            var newList = list.classmates; // assign the classmate list to newList

            Console.WriteLine("Implementing a ForLoop");
            Console.WriteLine(".....................");
            for (int i = 0; i < newList.Count; i++)
            {
                Console.WriteLine($"{newList[i]} is in postiton {i} in the List");
            }
            Program.CreateNewLine();  // calls the createnewline method in program.cs
        }
    }
}
