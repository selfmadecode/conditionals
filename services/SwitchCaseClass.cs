﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreFundamentals.conditionals
{
    class SwitchCaseClass
    {
        public static void SwitchCase()
        {
            var list = new ClassMemberList();
            var myList = list.classmates;
            Console.WriteLine("Implementing a SwitchCase...");

            foreach (var member in myList)
            {
                switch (member)
                {
                    case "Chisom":
                        Console.WriteLine("Chisom is a member");
                        break;
                    case "John":
                        Console.WriteLine("John is a member");
                        break;
                    case "Tochukwu":
                        Console.WriteLine("Tochukwu is the Governor");
                        break;
                }
            }


            /// Implementing a better switch case with for each
            List<string> fruitList = new List<string>()
            { "Apple", "Pear"};

            fruitList.ForEach(x =>
            {
                switch (x)
                {
                    case "Apple":
                        {
                            //do something
                            break;
                        }
                    case "Pear":
                        {
                            // do something
                            break;
                        }
                    default:
                        break;
                }
            });
        }
    }
}
