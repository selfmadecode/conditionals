﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreFundamentals.conditionals
{
    class WhileClass
    {
        public static void While()
        {
            var list = new ClassMemberList();
            var newList = list.classmates;
            Console.WriteLine("Implementing While Loop...");
            bool condition = true;

            while (condition) // while b is true
            {
                // generate class list before checking the condition
                for (int i = 0; i < newList.Count; i++)
                {
                    Console.WriteLine($"{newList[i]} is in postiton {i} in the List");
                }
                condition = false; // b is false, stops the loop
            }
            Program.CreateNewLine();

            
        }
    }
}
