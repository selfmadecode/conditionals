﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreFundamentals.conditionals
{
    class ForEachClass
    {
        public static void forEachMethod()
        {
            var newList = new ClassMemberList(); // Creates an instance of the ClassList
            var collection = newList.classmates; // the classmates list is assigned to collection
            collection.Add("SelfMade");

            Console.WriteLine("Implemeting a ForEach Loop!");
            Console.WriteLine(".....................");
            Console.WriteLine("Members in the list includes..");
            foreach (var item in collection)
            {
                Console.WriteLine(item);
            }
            Program.CreateNewLine();

            
        }
    }
}
