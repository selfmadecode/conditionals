﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreFundamentals.conditionals
{
    class IfStatement
    {
        public static void IfStateMent()
        {
            var list = new ClassMemberList();
            var newList = list.classmates;

            Console.WriteLine("Implementing an If Statement");
            newList.Add("Merit");
            Console.WriteLine("Merit has been added to list..");
            Console.WriteLine("Searching for list members....");

            if (!newList.Contains("Merit"))
            {
                Console.WriteLine("Merit is in the List");
            }
            else if (newList.Contains("John"))
            {
                Console.WriteLine("John is in the List");
            }
            else
            {
                Console.WriteLine("Done searching...");
            }
            Program.CreateNewLine();
        }
    }
}
