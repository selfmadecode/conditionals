﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreFundamentals.conditionals
{
    class DoWhileClass
    {
        public static void DoWhile()
        {
            var list = new ClassMemberList();
            var newList = list.classmates;
            Console.WriteLine("Implementing DoWhile");
            bool b = false;
            do
            {
                // generate class list before checking the condition
                for (int i = 0; i < newList.Count; i++)
                {
                    Console.WriteLine($"{newList[i]} is in postiton {i} in the List");
                }

            } while (b); // stops the do while loop because condition is false
            Program.CreateNewLine(); // create a new line

            // Guessing a number

            var randNum = new Random();
            var correctNum = randNum.Next(2, 11);
            var val = new Random();
            var rangeVal = val.Next(1, 4);
            var maxRange = (correctNum + rangeVal); // set an upper bound for the user to guess
            var minRange = (correctNum - rangeVal); // sets a lower bound

            bool con = true;
            do
            {
                try
                {
                    Console.Write("Guess a number between 1 and 10: ");
                    var num = Console.ReadLine();
                    int guessedNum = Convert.ToInt32(num);
                     Console.WriteLine(correctNum +" Correct num");
                    Console.WriteLine(rangeVal + " Range");
                    if (guessedNum == correctNum)
                    {
                        Console.WriteLine($"Correct, you've gotten it, {guessedNum} is the number");
                        con = false;
                    }
                    else
                    {
                        Console.WriteLine($"Incorrect, try again please, the correct number range is between {minRange} and {maxRange}");
                        con = true;
                    }
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                }
            } while (con);
        }
    }
}
